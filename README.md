# react-contacts

### Requirements
 - Create a simple SPA web application using React and any other tools you require. 
 - All code should be written completely by yourself
 - The application should feature a working contact list, feel free to take inspiration from existing designs but do not copy any code.
 - The contact application should display contact records and the ability display, edit, and delete the contact
 - The contact app should be able to add new contacts
 - Mock out contact data API so that a local JSON file is the data source, model this data yourself.
 - Wire up the contact data so that it is retrieved in your SPA web application
 - Be prepared to demo your UI and discuss the technical details of your design and implementation
 - Commit your code to a public Github repository that you can share with us.
 - Please submit this Github repository URL to us one day before scheduled interview day